package com.gustu.qiscuschallange.utils

interface Action<T>{
    fun call(t:T)
}