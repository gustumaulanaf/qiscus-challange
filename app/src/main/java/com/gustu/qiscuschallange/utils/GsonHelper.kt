package com.gustu.qiscuschallange.utils

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder

class GsonHelper {
    companion object {
        val gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }

}