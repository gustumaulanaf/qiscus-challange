package com.gustu.qiscuschallange.utils

import android.content.Context
import com.gustu.qiscuschallange.data.source.ChatRoomRepository
import com.gustu.qiscuschallange.data.source.UserRepository
import com.gustu.qiscuschallange.data.source.imp.ChatRoomImp
import com.gustu.qiscuschallange.data.source.imp.UserImp

class RepoUtils() {
    private val userRepository: UserRepository? = UserImp()
    private val chatRoomRepository: ChatRoomRepository? = ChatRoomImp()


    public final fun getChatRoomRepo(): ChatRoomRepository {
        return chatRoomRepository!!
    }

    public final fun getUserRepo(): UserRepository {
        return userRepository!!
    }
}