package com.gustu.qiscuschallange

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.gustu.qiscuschallange.utils.RepoUtils
import com.qiscus.sdk.chat.core.QiscusCore

public class MyApps : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        QiscusCore.setup(this, BuildConfig.QISCUS_APP_ID)
    }

    companion object {
        val repoUtil = RepoUtils()

        @get:Synchronized
        var context: Context? = null
            private set

         val instance: MyApps? = null


//        @get:Synchronized
//        var component: AppComponent? = null
    }


}