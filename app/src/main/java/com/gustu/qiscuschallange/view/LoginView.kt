package com.gustu.qiscuschallange.view

import com.qiscus.sdk.chat.core.data.model.QiscusAccount

interface LoginView {
    fun gotoHomePage()
    fun hideDialog()
    fun showDialog()
    fun onFailed(toString: String)
    fun logout()

}