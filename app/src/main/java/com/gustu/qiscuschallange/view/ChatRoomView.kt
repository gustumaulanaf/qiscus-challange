package com.gustu.qiscuschallange.view

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment

interface ChatRoomView {
    fun showChat(messages: MutableList<QiscusComment>?)
    fun showRoomInfo(qiscusChatRoom: QiscusChatRoom?)
    fun onError(error: String)
    fun messageSented(qiscusComment: QiscusComment)
    fun messageFailed(error: String)
}