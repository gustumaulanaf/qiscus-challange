package com.gustu.qiscuschallange.view

import com.gustu.qiscuschallange.data.model.User
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom

interface ContactView {
    fun showContact(t: List<User>)
    fun showError(t: Throwable)
    fun gotoChatActivity(t: QiscusChatRoom)
}