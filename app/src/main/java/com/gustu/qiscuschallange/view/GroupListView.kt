package com.gustu.qiscuschallange.view

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom

interface GroupListView {
    fun showGroupList(t: List<QiscusChatRoom>)
    fun showErrorMessage(error: String)
    fun createGroupSuccess(t: QiscusChatRoom)
    fun createGroupFailed(t: Throwable)
}