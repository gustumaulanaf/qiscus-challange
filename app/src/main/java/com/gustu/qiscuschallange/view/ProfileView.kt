package com.gustu.qiscuschallange.view

import com.qiscus.sdk.chat.core.data.model.QiscusAccount

interface ProfileView {
    fun showProfile(t: QiscusAccount)
    fun showError(t: Throwable)
    fun logout()
}