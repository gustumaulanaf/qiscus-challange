package com.gustu.qiscuschallange.view

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom

interface MainView {
    fun showChatRoom(t: List<QiscusChatRoom>)
    fun showError(toString: String)
    fun gotoChatRoom(t: QiscusChatRoom)
    fun gotoChatRoomGroup(chatRoom: QiscusChatRoom)
}