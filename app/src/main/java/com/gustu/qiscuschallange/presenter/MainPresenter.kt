package com.gustu.qiscuschallange.presenter

import com.gustu.qiscuschallange.data.source.ChatRoomRepository
import com.gustu.qiscuschallange.view.MainView
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom

class MainPresenter(var mainView: MainView, var chatRoomRepository: ChatRoomRepository) {
    fun getChatRoom() {
        chatRoomRepository.getChatRoom({ t ->
            mainView.showChatRoom(t)
        },
            { t -> mainView.showError(t.toString()) })
    }
    fun startChatRoom(chatRoom: QiscusChatRoom){
        if (chatRoom.isGroup){
            mainView.gotoChatRoomGroup(chatRoom)
        }
        else{
            mainView.gotoChatRoom(chatRoom)
        }
    }
}