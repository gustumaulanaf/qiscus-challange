package com.gustu.qiscuschallange.presenter

import android.util.Log
import com.gustu.qiscuschallange.data.model.User
import com.gustu.qiscuschallange.data.source.ChatRoomRepository
import com.gustu.qiscuschallange.data.source.UserRepository
import com.gustu.qiscuschallange.data.source.imp.ChatRoomImp
import com.gustu.qiscuschallange.data.source.imp.UserImp
import com.gustu.qiscuschallange.view.ContactView

class ContactPresenter(var contactView: ContactView, var userImp: UserRepository , var chatRoomImp: ChatRoomRepository) {
    fun getContact(page: Long, limit: Int, query: String) {
        userImp.getUsers(page, limit, query,
            { t ->
                val listUser: ArrayList<User> = ArrayList()
                for (i in t.indices) {
                    val user = User()
                    user.ava = t.get(i).avatar
                    user.name = t.get(i).username
                    user.id = "${t.get(i).email}"
                    listUser.add(user)
                }
                contactView.showContact(listUser)
            },
            { t -> contactView.showError(t) })
    }
    fun starChat(user:User){
        chatRoomImp.createChatRoom(user,{
            t -> contactView.gotoChatActivity(t)
        },{
            t -> contactView.showError(t)
        })
    }
}