package com.gustu.qiscuschallange.presenter

import com.gustu.qiscuschallange.data.source.ChatRoomRepository
import com.gustu.qiscuschallange.data.source.imp.ChatRoomImp
import com.gustu.qiscuschallange.view.ChatRoomView

class ChatRoomPresenter(var chatRoomView: ChatRoomView,var chatRoomImp: ChatRoomRepository) {
    fun getChat(roomId:Long){
        chatRoomImp.getChatRoomByMessage(roomId,
            {t ->
                chatRoomView.showChat(t.second)
                chatRoomView.showRoomInfo(t.first)
            },
            {
                t ->
                chatRoomView.onError(t.toString())
            })
    }
    fun sendChat(roomId: Long,message:String){
        chatRoomImp.sendMessage(roomId,message,
            {t ->
                chatRoomView.messageSented(t)
            },
            {t->
                chatRoomView.messageFailed(t.toString())
            })
    }
}