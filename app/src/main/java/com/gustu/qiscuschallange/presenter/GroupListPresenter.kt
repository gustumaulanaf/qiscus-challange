package com.gustu.qiscuschallange.presenter

import com.gustu.qiscuschallange.data.model.User
import com.gustu.qiscuschallange.data.source.ChatRoomRepository
import com.gustu.qiscuschallange.data.source.imp.ChatRoomImp
import com.gustu.qiscuschallange.view.GroupListView

class GroupListPresenter(var groupListView: GroupListView, var chatRoomImp: ChatRoomRepository) {
    fun getGroupList() {
        chatRoomImp.getChatRoom({ t -> groupListView.showGroupList(t) },
            { t -> groupListView.showErrorMessage(t.toString()) })
    }

    fun createGroup(groupName: String, members: List<User>) {
        chatRoomImp.createGroupChatRoom(groupName, members,
            { t -> groupListView.createGroupSuccess(t) },
            { t -> groupListView.createGroupFailed(t) })
    }
}