package com.gustu.qiscuschallange.presenter

import android.content.Context
import com.gustu.qiscuschallange.data.source.UserRepository
import com.gustu.qiscuschallange.data.source.imp.UserImp
import com.gustu.qiscuschallange.view.ProfileView

class ProfilePresenter(var profileView: ProfileView, var userImp: UserRepository){
    fun getProfile(){
        userImp.getUser({
            t ->
            profileView.showProfile(t)
        },
            {
                t ->
                profileView.showError(t)
            })
    }
    fun logout(context: Context){
        userImp.logout(context )
        profileView.logout()
    }
}