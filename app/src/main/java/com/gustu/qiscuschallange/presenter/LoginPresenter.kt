package com.gustu.qiscuschallange.presenter

import android.content.Context
import com.google.gson.Gson
import com.gustu.qiscuschallange.data.model.User
import com.gustu.qiscuschallange.data.source.UserRepository
import com.gustu.qiscuschallange.utils.Action
import com.gustu.qiscuschallange.utils.SharedPrefUtil
import com.gustu.qiscuschallange.view.LoginView
import com.qiscus.sdk.chat.core.QiscusCore
import rx.Emitter
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class LoginPresenter(var loginView: LoginView, var userRepository: UserRepository) {
    lateinit var gson: Gson
    fun login(email: String, pswd: String, ava: String, callName: String) {
        loginView.showDialog()
        userRepository.login(email, pswd, ava, callName,
            { user: User ->
                loginView.hideDialog()
                loginView.gotoHomePage()
            },
            { throwable: Throwable ->
                loginView.hideDialog()
                loginView.onFailed(throwable.message!!)
            })

    }

    fun getCurrentUsers(onSuccess: Action<User>, onFailed: Action<Throwable>) {
        getCurrentUserObservable()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun getCurrentUserObservable(): Observable<User?>? {
        return Observable.create<User?>({ subscriber: Emitter<User?> ->
            try {
                subscriber.onNext(getCurrentUser())
            } catch (e: Exception) {
                subscriber.onError(e)
            } finally {
                subscriber.onCompleted()
            }
        }, Emitter.BackpressureMode.BUFFER)
    }

    private fun getCurrentUser(): User? {
        return gson.fromJson(SharedPrefUtil.getString("user"), User::class.java)
    }

    private fun setCurrentUser(user: User) {
        SharedPrefUtil.saveString("user", gson.toJson(user))
    }

    fun logout(context: Context) {
        userRepository.logout(context)
        loginView.logout()
    }
}