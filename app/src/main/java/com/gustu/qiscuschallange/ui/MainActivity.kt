package com.gustu.qiscuschallange.ui

import RecyclerItemClickListener
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.gustu.qiscuschallange.MyApps
import com.gustu.qiscuschallange.R
import com.gustu.qiscuschallange.data.source.ChatRoomRepository
import com.gustu.qiscuschallange.data.source.imp.ChatRoomImp
import com.gustu.qiscuschallange.presenter.MainPresenter
import com.gustu.qiscuschallange.ui.adapter.MainAdapter
import com.gustu.qiscuschallange.view.MainView
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.event.QiscusCommentReceivedEvent
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

public class MainActivity : AppCompatActivity(), MainView {
    lateinit var mainPresenter: MainPresenter
    lateinit var mainAdapter: MainAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initPresenter()
        fabAddChat.setOnClickListener {
            startActivity(Intent(this, ContactActivity::class.java))
        }
    }

    public fun initPresenter() {
        mainPresenter = MainPresenter(this, MyApps.repoUtil.getChatRoomRepo()!!)
        mainPresenter.getChatRoom()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.top_main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_profile) {
            startActivity(Intent(this, ProfileActivity::class.java))
            finish()
        }
        return true
    }


    override fun showChatRoom(t: List<QiscusChatRoom>) {
        if (t.size != 0) {
            val contex = this.applicationContext
            mainAdapter = MainAdapter(t, this)
            mainAdapter.notifyDataSetChanged()
            rvChatRoom.adapter = mainAdapter
            rvChatRoom.layoutManager = LinearLayoutManager(this)
            rvChatRoom.hasFixedSize()
        }
    }

    override fun showError(toString: String) {
        Toast.makeText(applicationContext, toString, Toast.LENGTH_SHORT).show()
    }

    override fun gotoChatRoom(t: QiscusChatRoom) {
        startActivity(Intent(ChatActivity.intentParcelable(this, t)))
    }

    override fun gotoChatRoomGroup(chatRoom: QiscusChatRoom) {
    }

    override public fun onResume() {
        super.onResume()
        initPresenter()
        EventBus.getDefault().register(this)
    }

    override public fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onMessageRevieced(qiscusCommentReceivedEvent: QiscusCommentReceivedEvent) {
        mainPresenter.getChatRoom()
    }
}