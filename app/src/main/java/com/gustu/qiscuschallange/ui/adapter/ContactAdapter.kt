package com.gustu.qiscuschallange.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gustu.qiscuschallange.R
import com.gustu.qiscuschallange.data.model.User
import com.qiscus.sdk.chat.core.data.model.QiscusContact
import kotlinx.android.synthetic.main.item_rv_contact.view.*

class ContactAdapter(var userlist: List<User>, var context: Context) :
    RecyclerView.Adapter<ContactAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img = itemView.imgContact
        val name = itemView.tvNameContact
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_rv_contact, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userlist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = userlist.get(position)
        Glide.with(context).load(contact.ava).into(holder.img)
        holder.name.setText(contact.name)
    }
}