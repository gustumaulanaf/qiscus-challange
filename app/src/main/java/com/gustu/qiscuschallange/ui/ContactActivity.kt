package com.gustu.qiscuschallange.ui

import RecyclerItemClickListener
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.gustu.qiscuschallange.MyApps
import com.gustu.qiscuschallange.R
import com.gustu.qiscuschallange.data.model.User
import com.gustu.qiscuschallange.data.source.imp.ChatRoomImp
import com.gustu.qiscuschallange.data.source.imp.UserImp
import com.gustu.qiscuschallange.presenter.ContactPresenter
import com.gustu.qiscuschallange.ui.adapter.ContactAdapter
import com.gustu.qiscuschallange.view.ContactView
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import kotlinx.android.synthetic.main.activity_contact.*

class ContactActivity : AppCompatActivity(), ContactView {
    lateinit var contactAdapter: ContactAdapter
    lateinit var contactPresenter: ContactPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)
        initPresenter()
    }

    private fun initPresenter() {
        contactPresenter = ContactPresenter(this, MyApps.repoUtil.getUserRepo(), MyApps.repoUtil.getChatRoomRepo())
        contactPresenter.getContact(1, 100, "")
    }

    override fun showContact(t: List<User>) {
        contactAdapter = ContactAdapter(t, this)
        rvContact.adapter = contactAdapter
        rvContact.layoutManager = LinearLayoutManager(this)
        rvContact.hasFixedSize()
        rvContact.addOnItemTouchListener(RecyclerItemClickListener(this,rvContact,object : RecyclerItemClickListener.OnItemClickListener{
            override fun onItemClick(view: View, position: Int) {

                contactPresenter.starChat(t.get(position))
            }

            override fun onLongItemClick(view: View?, position: Int) {
            }

        }))
    }

    override fun showError(t: Throwable) {
        Log.e("CONTACT ERROR","${t}")
    }

    override fun gotoChatActivity(t: QiscusChatRoom) {
      startActivity(ChatActivity.intentParcelable(this,t))
    }
}