package com.gustu.qiscuschallange.ui.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gustu.qiscuschallange.R
import com.gustu.qiscuschallange.ui.MainActivity
import com.gustu.qiscuschallange.utils.DateUtil
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import com.qiscus.sdk.chat.core.data.model.QiscusContact
import kotlinx.android.synthetic.main.item_rv_chatlist.view.*
import java.util.*

class MainAdapter(var qiscusChatRoom: List<QiscusChatRoom>, var mainActivity: MainActivity) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nama = itemView.tvNameRoom
        val time = itemView.tvTime
        val lastComment = itemView.tvLastComment
        val img = itemView.imgRoom
        val status = itemView.imgStatusChatList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context!!).inflate(R.layout.item_rv_chatlist, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return qiscusChatRoom.size
    }

    override fun onBindViewHolder(holder: MainAdapter.ViewHolder, position: Int) {
        val room = qiscusChatRoom.get(position)
        if (!qiscusChatRoom.get(position).lastComment.caption.toString().equals("")) {
            holder.time.setText(DateUtil.getTimeStringFromDate(qiscusChatRoom.get(position).lastComment.time))
            Glide.with(mainActivity.applicationContext).load(room.avatarUrl).into(holder.img)
            holder.nama.setText(room.name)
            if (qiscusChatRoom.get(position).lastComment.isMyComment) {
                holder.lastComment.setText(qiscusChatRoom.get(position).lastComment.caption)
            } else {
                holder.lastComment.setText(
                    qiscusChatRoom.get(
                        position
                    ).lastComment.caption
                )

            }
            if (qiscusChatRoom.get(position).lastComment.state == QiscusComment.STATE_ON_QISCUS) {
                holder.status.setImageDrawable(
                    ContextCompat.getDrawable(
                        mainActivity.applicationContext,
                        R.drawable.ic_baseline_check_grey_24
                    )
                )
            } else if (qiscusChatRoom.get(position).lastComment.state == QiscusComment.STATE_READ) {
                holder.status.setImageDrawable(
                    ContextCompat.getDrawable(
                        mainActivity.applicationContext,
                        R.drawable.ic_baseline_check_circle_24
                    )
                )
            } else if (qiscusChatRoom.get(position).lastComment.state == QiscusComment.STATE_PENDING) {
                holder.status.setImageDrawable(
                    ContextCompat.getDrawable(
                        mainActivity.applicationContext,
                        R.drawable.ic_baseline_access_time_24
                    )
                )
            }else if (qiscusChatRoom.get(position).lastComment.state == QiscusComment.STATE_DELIVERED) {
                holder.status.setImageDrawable(
                    ContextCompat.getDrawable(
                        mainActivity.applicationContext,
                        R.drawable.ic_baseline_check_grey_24
                    )
                )
            }
            holder.itemView.setOnClickListener {
                mainActivity.initPresenter()
                mainActivity.mainPresenter.startChatRoom(qiscusChatRoom.get(position))
            }
        }
    }

}