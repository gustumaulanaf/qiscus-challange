package com.gustu.qiscuschallange.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gustu.qiscuschallange.R
import com.gustu.qiscuschallange.utils.DateUtil
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import kotlinx.android.synthetic.main.item_chat.view.*

class ChatAdapter(var listChat: List<QiscusComment>, var context: Context) :
    RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvChatOther = itemView.tvOtherChat
        val imgOther = itemView.imgOtherChat
        val layoutOther = itemView.layoutOtherChat
        val myLayout = itemView.layoutMyChat
        val myChat = itemView.tvMyChat
        val myImg = itemView.imgMyChat
        val layoutOtherMedia = itemView.layoutMedia
        val myLayoutMedia = itemView.layoutMyMedia
        val myTime = itemView.tvTimeMyChat
        val myStatus = itemView.imgStatus
        val otherTime = itemView.tvTimeOther
        val cardTanggal  = itemView.cardTanggal
        val tanggalChat = itemView.tvTanggalChat
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listChat.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position==listChat.size-1){
            holder.cardTanggal.visibility = View.VISIBLE
            holder.tanggalChat.setText(DateUtil.getDateStringFromDate(listChat.get(position).time))
        }
        else {
            if (position<listChat.size-1){
                if (DateUtil.getDateStringFromDate(listChat.get(position).time).equals(DateUtil.getDateStringFromDate(listChat.get(position+1).time))){
                    holder.cardTanggal.visibility = View.GONE
                }
                else{
                    holder.cardTanggal.visibility = View.VISIBLE
                    holder.tanggalChat.setText(DateUtil.getDateStringFromDate(listChat.get(position).time))
                }
            }
        }
        if (listChat.get(position).isMyComment) {
            holder.myLayout.visibility = View.VISIBLE
            holder.layoutOther.visibility = View.GONE
            if (listChat.get(position).isImage) {
                holder.myLayoutMedia.visibility = View.VISIBLE
                Glide.with(context).load(listChat.get(position).attachmentUri).into(holder.myImg)
                holder.myChat.setText(listChat.get(position).caption)
            } else {
                holder.myLayoutMedia.visibility = View.GONE
                holder.myChat.setText(listChat.get(position).message)

            }
            if (listChat.get(position).state == QiscusComment.STATE_ON_QISCUS) {
                holder.myStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_baseline_check_grey_24))
            }
            else if (listChat.get(position).state == QiscusComment.STATE_READ){
                holder.myStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_baseline_check_circle_24))
            }
            else if (listChat.get(position).state == QiscusComment.STATE_PENDING){
                holder.myStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_baseline_access_time_24))
            }
            else if (listChat.get(position).state == QiscusComment.STATE_DELIVERED){
                holder.myStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_baseline_check_grey_24))
            }

            holder.myTime.setText(DateUtil.getTimeStringFromDate(listChat.get(position).time))

        } else {
            holder.layoutOther.visibility = View.VISIBLE
            holder.myLayout.visibility = View.GONE
            if (listChat.get(position).isImage) {
                holder.layoutOtherMedia.visibility = View.VISIBLE
                Glide.with(context).load(listChat.get(position).attachmentUri).into(holder.imgOther)
                holder.tvChatOther.setText(listChat.get(position).caption)
            } else {
                holder.layoutOtherMedia.visibility = View.GONE
                holder.tvChatOther.setText(listChat.get(position).message)
            }
            holder.otherTime.setText(DateUtil.getTimeStringFromDate(listChat.get(position).time))

        }
    }
}