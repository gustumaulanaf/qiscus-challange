package com.gustu.qiscuschallange.ui

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.gustu.qiscuschallange.R
import com.gustu.qiscuschallange.data.source.imp.UserImp
import com.gustu.qiscuschallange.presenter.LoginPresenter
import com.gustu.qiscuschallange.utils.SharedPrefUtil
import com.gustu.qiscuschallange.view.LoginView
import com.qiscus.sdk.chat.core.QiscusCore
import com.qiscus.sdk.chat.core.data.model.QiscusAccount
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), LoginView {
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var loginPresenter: LoginPresenter
    lateinit var progressDialog: ProgressDialog
    val RC_SIGN_IN = 1
    override fun onStart() {
        super.onStart()
        if (SharedPrefUtil.getBoolean("googleLogin")) {
            val account = GoogleSignIn.getLastSignedInAccount(this)
            if (account != null) {
                updateUI(account)
            }
        } else {
            if (SharedPrefUtil.getString("password") != null) {
                val email = SharedPrefUtil.getString("email")!!
                val password = SharedPrefUtil.getString("password")!!
                val callName = SharedPrefUtil.getString("callName")!!
                Log.d("AKUN", "$email + $password + $callName")
                loginPresenter.login(email, password, "", callName)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initGoogle()
        initDialog()
        initPresenter()
        btLogin.setOnClickListener {
            if (et_email.text.toString().isEmpty()) {
                et_email.setError("Tidak boleh kosong!")
            } else if (et_displayname.text.toString().isEmpty()) {
                et_displayname.setError("Tidak boleh kosong!")
            } else if (et_password.text.toString().isEmpty()) {
                et_password.setError("Tidak boleh kosong!")
            } else {
                SharedPrefUtil.saveBoolean("googleLogin", false)
                SharedPrefUtil.saveString("email", et_email.text.toString())
                SharedPrefUtil.saveString("password", et_password.text.toString())
                SharedPrefUtil.saveString("callName", et_displayname.text.toString())

                var email = et_email.text.toString()
                var pswd = et_password.text.toString()
                var displayName = et_displayname.text.toString()
                loginPresenter.login(email, pswd, "", displayName)
            }
        }
        btGoogleSignIn.setOnClickListener {
            GoogleSign()
        }
    }

    private fun initGoogle() {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private fun GoogleSign() {
        val intent = mGoogleSignInClient.signInIntent
        startActivityForResult(intent, RC_SIGN_IN)
    }


    private fun initDialog() {
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setTitle("Tunggu")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
    }

    private fun initPresenter() {
        loginPresenter = LoginPresenter(this, UserImp())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN && resultCode == Activity.RESULT_OK) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data!!)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(task: Task<GoogleSignInAccount>) {
        try {
            val account = task.getResult(ApiException::class.java)
            updateUI(account)
        } catch (e: ApiException) {
            Log.w("GOOGLE SIGN IN LOGIN", "failed ${e}")
            updateUI(null)
        }
    }

    fun updateUI(account: GoogleSignInAccount?) {
        SharedPrefUtil.saveBoolean("googleLogin", true)
        val email = account!!.email.toString()
        val pswd = account!!.id.toString()
        val avatar = account!!.photoUrl
        val callName = account!!.displayName
        loginPresenter.login(email,pswd, avatar.toString()!!,callName!!)

    }

    override fun gotoHomePage() {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    override fun hideDialog() {
        progressDialog.hide()
    }

    override fun showDialog() {
        progressDialog.show()
    }

    override fun onFailed(toString: String) {
        loginPresenter.logout(this)
    }

    override fun logout() {
        Toast.makeText(applicationContext, "Anda Belum Login", Toast.LENGTH_SHORT).show()
    }
}