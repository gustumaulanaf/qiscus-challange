package com.gustu.qiscuschallange.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.gustu.qiscuschallange.MyApps
import com.gustu.qiscuschallange.R
import com.gustu.qiscuschallange.presenter.ChatRoomPresenter
import com.gustu.qiscuschallange.ui.adapter.ChatAdapter
import com.gustu.qiscuschallange.view.ChatRoomView
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import com.qiscus.sdk.chat.core.event.QiscusCommentReceivedEvent
import com.qiscus.sdk.chat.core.event.QiscusUserStatusEvent
import com.qiscus.sdk.chat.core.util.QiscusDateUtil
import kotlinx.android.synthetic.main.activity_chat.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

public class ChatActivity : AppCompatActivity(), ChatRoomView {
    lateinit var qiscusChatRoom: QiscusChatRoom
    lateinit var chatAdapter: ChatAdapter
    lateinit var chatRoomPresenter: ChatRoomPresenter

    companion object {
        val CHATROOM = "chat_room"
        fun intentParcelable(context: Context, t: QiscusChatRoom): Intent? {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra(CHATROOM, t)
            return intent
        }

    }

    override fun onResume() {
        super.onResume()
        initPresenter()
        EventBus.getDefault().register(this)
    }


    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        qiscusChatRoom = intent.getParcelableExtra(CHATROOM)!!
        initPresenter()
        Glide.with(applicationContext).load(qiscusChatRoom.avatarUrl).into(imgTarget)
        tvNameTarget.setText(qiscusChatRoom.name)
        btSendChat.setOnClickListener {
            if (!etChat.text.isEmpty()) {
                chatRoomPresenter.sendChat(qiscusChatRoom.id, etChat.text.toString())
            }
        }
        imgBackChat.setOnClickListener { finish() }


    }

    private fun initPresenter() {
        qiscusChatRoom = intent.getParcelableExtra(CHATROOM)!!
        chatRoomPresenter = ChatRoomPresenter(this, MyApps.repoUtil.getChatRoomRepo())
        chatRoomPresenter.getChat(qiscusChatRoom.id)
    }

    override fun showChat(messages: MutableList<QiscusComment>?) {
        Log.d("STATUSMESSAGE",messages?.get(messages.size-1)?.state.toString())
        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = false
        layoutManager.reverseLayout = true
        chatAdapter = ChatAdapter(messages!!, this)
        rvChat.adapter = chatAdapter
        rvChat.layoutManager = layoutManager
        rvChat.hasFixedSize()
        chatAdapter.notifyDataSetChanged()
    }

    override fun showRoomInfo(qiscusChatRoom: QiscusChatRoom?) {
    }

    override fun onError(error: String) {
        Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
    }

    override fun messageSented(qiscusComment: QiscusComment) {
        etChat.setText("")
        chatRoomPresenter.getChat(qiscusChatRoom.id)
        chatAdapter.notifyDataSetChanged()
    }

    override fun messageFailed(error: String) {
        Toast.makeText(applicationContext, "Gagal Terkirim", Toast.LENGTH_SHORT).show()
    }

    @Subscribe
    public fun onMessageRecieved(qiscusCommentReceivedEvent: QiscusCommentReceivedEvent) {
        chatRoomPresenter.getChat(qiscusChatRoom.id)
    }

    @Subscribe
    fun onUserStatusChanged(event: QiscusUserStatusEvent) {
        val last = QiscusDateUtil.getRelativeTimeDiff(event.lastActive)
        tvStatusTarget.setText(if (event.isOnline) "Online" else "Last seen $last")
        tvStatusTarget.setVisibility(View.VISIBLE)
    }


}