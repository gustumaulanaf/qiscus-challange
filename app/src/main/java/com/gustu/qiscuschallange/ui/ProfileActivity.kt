package com.gustu.qiscuschallange.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.gustu.qiscuschallange.MyApps
import com.gustu.qiscuschallange.R
import com.gustu.qiscuschallange.data.source.imp.UserImp
import com.gustu.qiscuschallange.presenter.ProfilePresenter
import com.gustu.qiscuschallange.view.ProfileView
import com.qiscus.sdk.chat.core.data.model.QiscusAccount
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity(), ProfileView {
    lateinit var profilePresenter: ProfilePresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        initPresenter()
        btLogout.setOnClickListener {
            profilePresenter.logout(this)
        }

    }

    private fun initPresenter() {
        profilePresenter = ProfilePresenter(this, MyApps.repoUtil.getUserRepo())
        profilePresenter.getProfile()
    }

    override fun showProfile(t: QiscusAccount) {
        Log.d("PROFILEUSER",t.username + t.avatar + t.email)
        tvUserProfile.setText(t.username)
        tvEmailProfile.setText(t.email)
        Glide.with(applicationContext).load(t.avatar).into(fotoProfile)
    }

    override fun showError(t: Throwable) {
        Toast.makeText(applicationContext, "${t}", Toast.LENGTH_SHORT).show()
    }

    override fun logout() {
        val intent = Intent(applicationContext, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        return true
    }
}