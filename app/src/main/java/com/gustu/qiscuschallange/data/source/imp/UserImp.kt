package com.gustu.qiscuschallange.data.source.imp

import android.content.Context
import android.util.Log
import com.firebase.ui.auth.AuthUI.getApplicationContext
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.gustu.qiscuschallange.data.model.User
import com.gustu.qiscuschallange.data.source.UserRepository
import com.gustu.qiscuschallange.utils.AvatarUtil
import com.gustu.qiscuschallange.utils.GsonHelper
import com.gustu.qiscuschallange.utils.SharedPrefUtil
import com.qiscus.sdk.chat.core.QiscusCore
import com.qiscus.sdk.chat.core.data.model.QiscusAccount
import com.qiscus.sdk.chat.core.data.remote.QiscusApi
import org.json.JSONObject
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class UserImp() :
    UserRepository {
    override fun login(
        email: String,
        pswd: String,
        ava: String,
        callName: String,
        onSuccess: (User) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val avatar: String
        val user = User()
        user.name = callName
        if (ava.isEmpty()) {
            user.ava = AvatarUtil.generateAvatar(callName)
            avatar = AvatarUtil.generateAvatar(callName)
        } else {
            user.ava = ava
            avatar = ava
        }

        val jsonObjects = JSONObject()
        QiscusCore.setUser(email, pswd)
            .withUsername(callName)
            .withAvatarUrl(avatar)
            .withExtras(jsonObjects)
            .save()
            .map(this::mapQiscus)
            .doOnNext(this::setCurrentUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    override fun logout(context: Context) {

        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
        mGoogleSignInClient.signOut()
        QiscusCore.removeDeviceToken(SharedPrefUtil.getToken("token"))
        QiscusCore.clearUser()
        SharedPrefUtil.edit().clear().apply()
    }

    override fun getUsers(
        page: Long,
        limit: Int,
        query: String,
        onSuccess: (List<QiscusAccount>) -> Unit,
        onError: (Throwable) -> Unit
    ) {

        QiscusApi.getInstance().getUsers(query, page, limit.toLong())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
//
//        QiscusApi.getInstance().getUsers(query, page, limit.toLong())
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(
//                { qiscusAccounts: List<QiscusAccount?>? ->
//                  onSuccess
//                }
//            ) { throwable: Throwable? ->
//                Log.d("LIST ACCOUNT", throwable.toString())
//            }
    }

    private fun getCurrentUser(): User? {
        return GsonHelper.gson.fromJson(SharedPrefUtil.getString("current_user"), User::class.java)
    }

    private fun setCurrentUser(user: User) {
        SharedPrefUtil.saveString("current_user", GsonHelper.gson.toJson(user))
    }

    fun mapQiscus(qiscusAccount: QiscusAccount): User {
        Log.d("ACCOUNT QISCUS", qiscusAccount.username)
        val user = User()
        user.id = qiscusAccount.email
        user.name = qiscusAccount.username
        user.ava = qiscusAccount.avatar
        SharedPrefUtil.saveToken("token", qiscusAccount.token!!)
        return user
    }

    override fun getUser(onSuccess: (QiscusAccount) -> Unit, onError: (Throwable) -> Unit) {
        try {
            onSuccess(QiscusCore.getQiscusAccount())
        } catch (e: Throwable) {
            onError(e)

        }
    }


}
