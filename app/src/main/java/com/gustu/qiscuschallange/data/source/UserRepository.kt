package com.gustu.qiscuschallange.data.source

import android.content.Context
import com.gustu.qiscuschallange.data.model.User
import com.qiscus.sdk.chat.core.data.model.QiscusAccount

interface UserRepository {
    //    fun login (email:String,pswd:String,callName:String,onSuccess:Action<User>,onError:Action<Throwable>)
    fun login(
        email: String,
        pswd: String,
        ava:String,
        callName: String,
        onSuccess: (User) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun getUsers(
        page: Long,
        limit: Int,
        query: String,
        onSuccess: (List<QiscusAccount>) -> Unit,
        onError: (Throwable) -> Unit
    )
    fun getUser(onSuccess:(QiscusAccount) ->Unit , onError:(Throwable) -> Unit)

    fun logout(context: Context)
}