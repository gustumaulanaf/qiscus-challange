package com.gustu.qiscuschallange.data.source

import androidx.core.util.Pair
import com.gustu.qiscuschallange.data.model.User
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import rx.functions.Action1

interface ChatRoomRepository {

    fun getChatRoom(onSuccess: (List<QiscusChatRoom>) -> Unit, onError: (Throwable) -> Unit)
    fun createChatRoom(
        user: User,
        onSuccess: (QiscusChatRoom) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun createGroupChatRoom(
        groupName: String,
        members: List<User>,
        onSuccess: (QiscusChatRoom) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun addMember(
        roomId: Long,
        members: List<User>,
        onSuccess: (Void) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun kickMember(
        roomId: Long,
        members: List<User>,
        onSuccess: (Void) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun getChatRoomByMessage(
        roomId: Long,
        onSuccess: (Pair<QiscusChatRoom, MutableList<QiscusComment>>) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun sendMessage(
        roomId: Long,
        message: String,
        onSucces: (QiscusComment) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun chatUser(userId: String, onSuccess: (QiscusChatRoom) -> Unit, onError: (Throwable) -> Unit)

}