package com.gustu.qiscuschallange.data.source.imp

import android.util.Log
import androidx.core.util.Pair
import com.gustu.qiscuschallange.data.model.User
import com.gustu.qiscuschallange.data.source.ChatRoomRepository
import com.gustu.qiscuschallange.utils.AvatarUtil
import com.gustu.qiscuschallange.utils.GsonHelper
import com.gustu.qiscuschallange.utils.SharedPrefUtil
import com.qiscus.sdk.chat.core.QiscusCore
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import com.qiscus.sdk.chat.core.data.remote.QiscusApi
import org.json.JSONObject
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action1
import rx.schedulers.Schedulers

class ChatRoomImp : ChatRoomRepository {
    override fun getChatRoom(
        onSuccess: (List<QiscusChatRoom>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        Observable.from(QiscusCore.getDataStore().getChatRooms(100))
            .filter { t -> t.lastComment != null }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
        QiscusApi.getInstance().getAllChatRooms(true, false, true, 1, 100)
            .flatMap<QiscusChatRoom> { iterable: List<QiscusChatRoom?>? -> Observable.from(iterable) }
            .doOnNext { t -> QiscusCore.getDataStore().addOrUpdate(t) }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    override fun createChatRoom(
        user: User,
        onSuccess: (QiscusChatRoom) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val gson = GsonHelper.gson.toJson(user)
        val jsonObjects = JSONObject()
        Log.d("USERID", user.id!!)

        QiscusApi.getInstance()
            .chatUser(user.id, jsonObjects)
            .doOnNext { t -> QiscusCore.getDataStore().addOrUpdate(t) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    override fun createGroupChatRoom(
        groupName: String,
        members: List<User>,
        onSuccess: (QiscusChatRoom) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val listId: ArrayList<String> = ArrayList()
        listId.clear()
        for (i in members.indices) {
            listId.add(members.get(i).id!!)
        }
        QiscusApi.getInstance()
            .createGroupChat(groupName, listId, AvatarUtil.generateAvatar(groupName), null)
            .doOnNext { t -> QiscusCore.getDataStore().addOrUpdate(t) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }


    override fun addMember(
        roomId: Long,
        members: List<User>,
        onSuccess: (Void) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val listId: ArrayList<String> = ArrayList()
        listId.clear()
        for (i in members.indices) {
            listId.add(members.get(i).id!!)
        }
        QiscusApi.getInstance()
            .addParticipants(roomId, listId)
            .doOnNext { t -> QiscusCore.getDataStore().addOrUpdate(t) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { t: QiscusChatRoom? ->
                onSuccess;onError
            }
    }

    override fun kickMember(
        roomId: Long,
        members: List<User>,
        onSuccess: (Void) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val listId: ArrayList<String> = ArrayList()
        listId.clear()
        for (i in members.indices) {
            listId.add(members.get(i).id!!)
        }
        QiscusApi.getInstance()
            .removeParticipants(roomId, listId)
            .doOnNext { t -> QiscusCore.getDataStore().addOrUpdate(t) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Action1 { t: QiscusChatRoom -> onSuccess }
                onError
            }
    }


    override fun getChatRoomByMessage(
        roomId: Long,
        onSuccess: (Pair<QiscusChatRoom, MutableList<QiscusComment>>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        QiscusApi.getInstance().getChatRoomWithMessages(roomId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }

    override fun sendMessage(
        roomId: Long,
        message: String,
        onSucces: (QiscusComment) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val qiscusMessage = QiscusComment.generateMessage(roomId, message)
        QiscusApi.getInstance().sendMessage(qiscusMessage)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSucces, onError)
    }

    override fun chatUser(
        userId: String,
        onSuccess: (QiscusChatRoom) -> Unit,
        onError: (Throwable) -> Unit
    ) {
//        val gsonString = GsonHelper.gson.toJson(userId)
//        val jsonObject = JSONObject(gsonString)
        QiscusApi.getInstance().chatUser(userId, null).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
    }


}