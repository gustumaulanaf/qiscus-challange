package com.gustu.qiscuschallange.data.model

import android.os.Parcel
import android.os.Parcelable

class User() : Parcelable, Comparable<User> {
    var id: String? = ""
    var name: String? = ""
    var ava:String?=""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        ava = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(ava)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

    override fun compareTo(other: User): Int {
        return name!!.compareTo(other.name!!)
    }
}